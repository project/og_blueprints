<?php

/**
 * @file helptext.inc
 * 
 * Include file specifically containing the vast swathes of inline 
 * help messages that blueprints employs to make the process more intelligible 
 * 
 * Function naming structure is as follows for inline text:
 * 
 *  "ogbp_help_" + (section) + "_" + (item/group)
 * 
 * And for hook_help()-based help messages:
 * 
 *  "ogbp_helphook_" + (section) + "_" + (item/group)
 */

function ogbp_help_dashboard_inline(&$t_args, $location, $bundle, $args = array()) {
  if ($location == 'bundlecfg') {
    list($help_text[2][], $help_text[1][], $help_text[0][]) = array_fill(0, 3, "This table allows you to change those blueprint settings that are not connected to the blueprint's panel configuration. For more thorough explanations of each setting, see the corresponding !bundlesettings.");
    list($t_args[2]['!bundlesettings'], $t_args[1]['!bundlesettings'], $t_args[0]['!bundlesettings']) = array_fill(0, 3, l(t('help page'), 'help...')); // FIXME finish/figure it out
    
    $help_text[1][] = "However, because you are viewing the Master bundle in 'Bundles Per Group Type' mode, almost all of the widgets for these settings are disabled, as these Master values are neither used nor referenced in this mode. A more detailed explanation of the logic behind this behavior can be found !masterlogic.";
    $t_args[1]['!masterlogic'] = l(t('in the documentation'), 'help...'); // FIXME finish/figure it out
    $help_text[1][] = "The only unique functionality on this page is deleting blueprints. Please note, however, that deleting a blueprint here will delete that blueprint for <strong>ALL</strong> your bundles. If you don't want a blueprint to be instantiated with a particular group type, you should simply <em>disable</em> it in that bundle's settings.";
    
    $help_text[2][] = "Every setting visible on this page, as well as every setting on the add/edit blueprint editing form (accessible via the 'Edit' links in the table) are specific to this bundle ONLY. They are completely unrelated to linking.";
    $t_args[2]['!editblueprints'] = '';
  }
  elseif ($location == 'blueprintsetup') {
    list($help_text[2][], $help_text[1][], $help_text[0][]) = array_fill(0, 3, "Use the links in the table below to edit the panels settings - content, layout, and layout settings - for the blueprints that comprise your bundle.");
    
    $help_text[1][] = "You're editing the Master bundle right now, so your changes will 'propogate' to all the other bundles' blueprints that are linked to the master. The number of blueprints still linked is listed in the 'Linked Blueprints' column; a meager UI offering, but more features are on the way soon. You can also learn more about the the Blueprints linking system works in the !linking (soon!).";    
    $help_text[2][] = "Because you are editing a type-specific bundle with OG Blueprints in Bundle Per Type mode, you have to think about blueprint linking. The !linking has a more detailed explanation, but the essentials are these: all you have to do delink a blueprint is go into one of the panels editors (via the links below) and save some changes; as the messages will tell you, the delinking process ONLY begins once you've clicked 'Save.'";
    $help_text[2][] = "While you can relink the blueprint to the Master at any time, doing so too much could prove be disruptive to your users. Linking and delinking a blueprint back and forth from one release to the next can easily result a plethora of confusing update messages for your group admins.";
    $t_args[1]['!linking'] = $t_args[2]['!linking'] = '';
  }
  elseif ($location = 'mode') {
    list($help_text[1][], $help_text[0][], $help_text[-1][]) = array_fill(0, 3, "Group 'blueprints' are bundles of panels and administrative settings, encompassing all the options a group administrator is able to manually set for their own group using OG Panels. Rather than applying to just one group, however, these bundles & blueprints are defined by group <em>type</em>. The blueprints get instantiated as actual OG Panels when new groups get created.");
    $help_text[-1][] = "OG Blueprints allows varying levels of granularity in configuring these blueprints. However, bundles are currently disabled. Select one of the 'Enabled' settings below to begin bundles. <ul><li>Selecting 'Enabled - One Bundle' will cause ALL of your group types to use the same bundle.</li><li>Selecting 'Enabled - Bundles Per Type' will allow you to create bundles for each group type you have defined in !ogadmin.</li></ul>";
    $t_args[-1]['!ogadmin'] = $t_args[0]['!ogadmin'] = '';
    $help_text[0][] = "OG Blueprints is currently operating in 'One Bundle' mode, which means the blueprints in the bundle you define will apply to ALL the node types you define as groups in !ogadmin.";
    $help_text[0][] = "You can switch to 'Bundles Per Type' mode without losing any of your settings. If you disable OG Blueprints, your configuration will be exactly the same when it is re-enabled.";
    $help_text[1][] = "OG Blueprints is currently operating in 'Bundles Per Type' mode. This mode is quite complex, and it's recommended that you spend some time familiarizing yourself with the !linking (available soon) before you start making releases.";
    $t_args[1]['!linking'] = ''; 
    $help_text[1][] = "<strong>WARNING:</strong> Switching back to 'One Bundle' has not yet been thoroughly tested, and there is consequently the potential that some of your type-specific bundles & blueprints could be altered or destroyed if you switch back. However, there is no risk of damaging your data UNTIL you make a release in this mode.";
    $help_text[1][] = "It is the module developer's recommendation that, if you have any thoughts that you might like to have different blueprints for different group types at any time, you operate in 'Bundles Per Type' mode from the very beginning.";
  }
  return _ogbp_finish($help_text, $t_args);
}


function ogbp_help_dashboard_caption(&$t_args, $location, $bundle, $args = array()) {
  $text = '<strong>PLEASE NOTE:</strong> Until you create a release for this bundle, it will be inactive - your blueprints will NOT be instantiated as OG Panels when a ';
  if (empty($bundle->release_id)) {
    $help_text[0][] = $text . 'group is created.';
    $help_text[2][] = $text . "'$bundle->grouptype' group is created.";
  }
  else {
    $help_text[0][] = $help_text[2][] = '';
  }
  $help_text[1][] = "You cannot create releases of the Master bundle in 'Bundle-Per-Type' mode. Only releases of the type-specific bundles can be created.";  
  return _ogbp_finish($help_text, $t_args);
}

function ogbp_help_mainform_caption(&$t_args, $location, $bundle, $args = array()) {
  if ($location == 'blueprintsetup') {
    $help_text[0][] = 'Disabled blueprints can be re-enabled in the !bundlecfg menu.';
    $t_args[0]['bundlecfg'] = '';
    $help_text[1][] = "<strong>Reminder:</strong> when you edit Master blueprints, you're changing those values for ALL of the blueprints that are still linked to the Master!";
    $help_text[2][] = "Never worry about delinking a blueprint from the Master blueprint - it's easily reversed, and nothing is deleted. Relinking, however, <strong>is</strong> a destructive process: the delinked display of the blueprint is deleted from the database, and can only be recovered if you have a !backuplink" . ".";
    $t_args[2]['!backuplink'] = l(t('backup of your database'), 'http://drupal.org/project/backup_migrate'); 
  }
  elseif ($location == 'bundlecfg') {
    $help_text[0][] = "Keep in mind that these settings are only defaults!  Once the blueprints are instantiated as og panels for a group, that group's' administrators will be as free to change these settings as the rest of their permissions allow.";
    $help_text[1][] = "The options in this field are disabled because each group type defines its own Bundle settings in Bundles-Per-Type mode, completely independent of the Master.";
    $help_text[2][] = "If you absolutely must delete a blueprint, then you'll have to go to the Master !bundlecfg to do it.";
    $t_args[2]['!bundlecfg'] = '';
  }
  return _ogbp_finish($help_text, $t_args);
}

function ogbp_help_mode_submit(&$t_args, $location, $bundle, $args = array()) {
  $help_text[-1][] = "Group Blueprints have been disabled. Any blueprints you have created will be accessible if you reactivate Group Blueprints.";
  $help_text[0][] = "Group Blueprints have been enabled, using one bundle for all group types. You can configure your bundle in !bundlecfg.";
  $help_text[1][] = "Group Blueprints have been enabled with one bundle per group type. You can configure all of your bundles in !bundlecfg.";
  $t_args[0]['!bundlecfg'] = $t_args[1]['!bundlecfg'] = '';
  return _ogbp_finish($help_text, $t_args);
}
/**
 * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 *    HELPER FUNCTIONS
 */

/**
 * Defines a commonly used set of links. Intended to be called
 * as the second argument in an array_merge.
 */
function _ogbp_full_links() {
  return $fullinks = array(
    '!editblueprints' => l(t('Edit Blueprints'), 'admin/og/og_blueprints/blueprintsetup'),
    '!bundlecfg' => l(t('Bundle Settings'), 'admin/og/og_blueprints/bundlecfg'),
    '!linking' => l(t('linking documentation'), ''),
    '!ogadmin' => l(t('Organic Groups Configuration'), 'admin/og/og'),
  );
}

function _ogbp_finish($help_text, &$t_args) {
  foreach ($help_text as $op => $help_items) { // ensures we keep the top-level array separate from the recursion.
    $help_text[$op] = _wrap(implode('</p><p>', $help_items));
  }
  $full_links = _ogbp_full_links();
  if (!empty($t_args)) {
    foreach ($t_args as $key => $links) {
    $t_args[$key] = array_merge($links, array_intersect_key($full_links, $links));
    }
  }
  else $t_args = array();
  return $help_text;
}

/**
 * Simply wraps the string with a particular HTML tag. Defaults
 * to <p>
 */
function _wrap($string, $tag = 'p') {
  return "<$tag>" . $string . "</$tag>";
}
