
Organic Group Blueprints
============================

OG Blueprints allows site administrators to create 'bundles' of OG panels 'blueprints.' Blueprints are identical to OG Panels
themselves in terms of configuration, but instead of being applied to individual groups, they are applied to group _types_. 
Once the site administrator 'releases' a bundle of blueprints, copies of those panel blueprints will automatically be added to
new groups upon creation.

Because the concepts that drive Blueprints are as abstract as they are, I'm of the opinion that there isn't much first-time
users of OG Blueprints will get out of my using a bunch of jargon to describe the module here in the README. So, I've taken
the alternate approach of providing most of the help text inline, contextually adjusted to the particular menu you're looking
at. I'm hoping it's enough to provide a basic step-by-step here that'll get you into the gist of that inline helptext:

1. If you haven't already, install Organic groups (http://drupal.org/project/og) via the normal drupal method. You MUST be 
   using at LEAST version 6.0 of OG.
2. If you havent' already, install OG Panels (included in the Organic groups module package) via the normal drupal method.
3. Install OG Blueprints via the normal drupal method.
  3a. Former users of OG Collections should skip to the below section. DO IT NOW. VERY UGLY, IRREPARABLE THINGS WILL 
      PROBABLY HAPPEN TO YOUR DATA IF YOU ADVANCE TO STEP 4 BEFORE COMPLETING THAT SECTION. 
4. If you haven't already, mark at least one content type as being a group on the og administration page (/admin/og/og).
5. Navigate to /admin/og/og_blueprints/, and select your preferred mode of operation.

From here on out, you're in the thick of the inline help and it ought to be much more useful than anythign I can write here.


=============================
FOR FORMER USERS OF OG COLLECTIONS

Don't worry, you won't lose your old Collections data. OG Collections has also had a new release, labelled TERMINAL, and the
.install file has a new upgrade script in it. Download the new package (http://ftp.drupal.org/files/projects/og_collections-5.x-1.0-terminal.tar.gz),
and head over to /update.php (AFTER you've installed OG Blueprints in /admin/build/modules). Go throught the module update
process, ensuring that you're updating to version 5002 of OG Collections. You should see log messages reflecting all of your
existing Collections data being migrated into the new Blueprints format.  